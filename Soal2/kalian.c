#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int main() {
    int mat1[ROW1][COL1], mat2[ROW2][COL2], product[ROW1][COL2];
    int i, j, k;

    // Inisialisasi matriks dengan angka random
    srand(time(NULL));
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            mat1[i][j] = rand() % 5 + 1;
        }
    }
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            mat2[i][j] = rand() % 4 + 1;
        }
    }

    // Hitung perkalian matriks
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            product[i][j] = 0;
            for (k = 0; k < ROW2; k++) {
                product[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    // Tampilkan matriks hasil perkalian
    printf("Hasil perkalian matriks:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d ", product[i][j]);
        }
        printf("\n");
    }

    // Buat shared memory untuk hasil perkalian matriks
    const char *shared_mem_name = "MySharedMemory1";
    int shm_fd = shm_open(shared_mem_name, O_CREAT | O_RDWR, 0666);
    if (shm_fd == -1) {
        printf("Error creating shared memory.\n");
        return 1;
    }
    ftruncate(shm_fd, sizeof(product));
    int *shared_mem = mmap(0, sizeof(product), PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shared_mem == MAP_FAILED) {
        printf("Error mapping shared memory.\n");
        shm_unlink(shared_mem_name);
        return 1;
    }

    // Salin hasil perkalian matriks ke shared memory
    memcpy(shared_mem, product, sizeof(product));

    // Tutup handle dan unmap shared memory
    munmap(shared_mem, sizeof(product));
    close(shm_fd);

    return 0;
}
