#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int product[ROW1][COL2]; // shared memory untuk hasil perkalian matriks
int fact[ROW1][COL2];    // shared memory untuk hasil faktorial

// Fungsi untuk menghitung faktorial
int factorial(int num)
{
    int f = 1;
    int i;
    for (i = 2; i <= num; i++)
    {
        f *= i;
    }
    return f;
}

int main()
{
    clock_t start_time, end_time;
    start_time = clock();

    int i, j;
    int shm_fd1, shm_fd2;
    int *shared_mem1, *shared_mem2;
    const char *shared_mem_name1 = "MySharedMemory1";
    const char *shared_mem_name2 = "MySharedMemory2";

    // Inisialisasi matriks dengan angka random
    srand(time(NULL));
    for (i = 0; i < ROW1; i++)
    {
        for (j = 0; j < COL1; j++)
        {
            product[i][j] = rand() % 5 + 1;
        }
    }
    for (i = 0; i < ROW2; i++)
    {
        for (j = 0; j < COL2; j++)
        {
            product[i][j] = rand() % 4 + 1;
        }
    }

    // Buka shared memory untuk hasil perkalian matriks
    shm_fd1 = shm_open(shared_mem_name1, O_CREAT | O_RDWR, 0666);
    if (shm_fd1 == -1)
    {
        printf("Error creating shared memory.\n");
        return 1;
    }
    ftruncate(shm_fd1, sizeof(product));
    shared_mem1 = mmap(0, sizeof(product), PROT_WRITE, MAP_SHARED, shm_fd1, 0);
    if (shared_mem1 == MAP_FAILED)
    {
        printf("Error mapping shared memory.\n");
        close(shm_fd1);
        return 1;
    }
    // Salin hasil perkalian matriks ke shared memory
    memcpy(shared_mem1, product, sizeof(product));

    // Buka shared memory untuk hasil faktorial
    shm_fd2 = shm_open(shared_mem_name2, O_CREAT | O_RDWR, 0666);
    if (shm_fd2 == -1)
    {
        printf("Error creating shared memory.\n");
        close(shm_fd1);
        return 1;
    }
    ftruncate(shm_fd2, sizeof(fact));
    shared_mem2 = mmap(0, sizeof(fact), PROT_WRITE, MAP_SHARED, shm_fd2, 0);
    if (shared_mem2 == MAP_FAILED)
    {
        printf("Error mapping shared memory.\n");
        close(shm_fd1);
        close(shm_fd2);
        return 1;
    }
    for (i = 0; i < ROW1; i++)
    {
        for (j = 0; j < COL2; j++)
        {
            int num = product[i][j];
            int f = factorial(num);
            fact[i][j] = f;
        }
    }
    // Cetak matriks hasil perkalian dan faktorial
    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROW1; i++)
    {
        for (j = 0; j < COL2; j++)
        {
            printf("%d ", shared_mem1[i * COL2 + j]);
        }
        printf("\n");
    }
    printf("\nMatriks hasil faktorial:\n");
    for (i = 0; i < ROW1; i++)
    {
        for (j = 0; j < COL2; j++)
        {
            printf("%d ", fact[i][j]);
        }
        printf("\n");
    }

    // Tutup shared memory
    munmap(shared_mem1, sizeof(product));
    munmap(shared_mem2, sizeof(fact));
    close(shm_fd1);
    close(shm_fd2);
    shm_unlink(shared_mem_name1);
    shm_unlink(shared_mem_name2);

    end_time = clock();
    double elapsed_time = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang diperlukan: %.3f s\n", elapsed_time);

    return 0;
}
