#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h> 
#include <unistd.h>
#include <string.h>
#include <time.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5
#define MAX_THREADS ROW1*COL2

int product[ROW1][COL2];
int fact[ROW1][COL2];

void *factorial(void *arg) {
    int *num = (int *)arg;
    int f = 1;
    int i;
    for (i = 2; i <= *num; i++) {
        f *= i;
    }
    *num = f;
    return NULL;
}

int main() {
    clock_t start_time, end_time;
    start_time = clock();
    int i, j;
    int shm_fd1, shm_fd2;
    int *shared_mem1, *shared_mem2;
    const char *shared_mem_name1 = "MySharedMemory1";
    const char *shared_mem_name2 = "MySharedMemory2";

    srand(time(NULL));
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            product[i][j] = rand() % 5 + 1;
        }
    }
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            product[i][j] = rand() % 4 + 1;
        }
    }

    shm_fd1 = shm_open(shared_mem_name1, O_CREAT | O_RDWR, 0666);
    if (shm_fd1 == -1) {
        printf("Error creating shared memory.\n");
        return 1;
    }
    ftruncate(shm_fd1, sizeof(product));
    shared_mem1 = mmap(0, sizeof(product), PROT_WRITE, MAP_SHARED, shm_fd1, 0);
    memcpy(shared_mem1, product, sizeof(product));

    shm_fd2 = shm_open(shared_mem_name2, O_CREAT | O_RDWR, 0666);
    if (shm_fd2 == -1) {
        printf("Error creating shared memory.\n");
        shm_unlink(shared_mem_name1);
        return 1;
    }
    ftruncate(shm_fd2, sizeof(fact));
    shared_mem2 = mmap(0, sizeof(fact), PROT_WRITE, MAP_SHARED, shm_fd2, 0);

    pthread_t threads[MAX_THREADS];
    int thread_args[MAX_THREADS];
    int thread_count = 0;
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            thread_args[thread_count] = product[i][j];
            pthread_create(&threads[thread_count], NULL, factorial, &thread_args[thread_count]);
            thread_count++;
        }
    }

    for (i = 0; i < MAX_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

     for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            fact[i][j] = thread_args[i * COL2 + j];
        }
    }

    memcpy(shared_mem2, fact, sizeof(fact));

    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d\t", shared_mem1[i * COL2 + j]);
        }
        printf("\n");
    }

    printf("\nMatriks hasil faktorial:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d\t", fact[i][j]);
        }
        printf("\n");
    }

    munmap(shared_mem1, sizeof(product));
    munmap(shared_mem2, sizeof(fact));
    shm_unlink(shared_mem_name1);
    shm_unlink(shared_mem_name2);

    end_time = clock();  
    double elapsed_time = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang diperlukan: %.3f s\n", elapsed_time);
    
    return 0;
}
