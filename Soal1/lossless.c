#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_CHAR 256

typedef struct node {
    char symbol;
    int freq;
    struct node *left;
    struct node *right;
} Node;

typedef struct heap {
    Node **arr;
    int size;
} Heap;

typedef struct code {
    char symbol;
    char *bits;
} Code;

void swap(Node **a, Node **b) {
    Node *tmp = *a;
    *a = *b;
    *b = tmp;
}

void heapify(Heap *heap, int i) {
    int smallest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < heap->size && heap->arr[left]->freq < heap->arr[smallest]->freq) {
        smallest = left;
    }

    if (right < heap->size && heap->arr[right]->freq < heap->arr[smallest]->freq) {
        smallest = right;
    }

    if (smallest != i) {
        swap(&heap->arr[i], &heap->arr[smallest]);
        heapify(heap, smallest);
    }
}

Heap *create_heap() {
    Heap *heap = malloc(sizeof(Heap));
    heap->arr = malloc(MAX_CHAR * sizeof(Node *));
    heap->size = 0;
    return heap;
}

void insert_heap(Heap *heap, Node *node) {
    heap->arr[heap->size++] = node;

    int i = heap->size - 1;
    while (i > 0 && heap->arr[i]->freq < heap->arr[(i - 1) / 2]->freq) {
        swap(&heap->arr[i], &heap->arr[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

Node *extract_min(Heap *heap) {
    Node *min = heap->arr[0];
    heap->arr[0] = heap->arr[--heap->size];
    heapify(heap, 0);
    return min;
}

Node *create_node(char symbol, int freq) {
    Node *node = malloc(sizeof(Node));
    node->symbol = symbol;
    node->freq = freq;
    node->left = NULL;
    node->right = NULL;
    return node;
}

void build_tree(Heap *heap) {
    while (heap->size > 1) {
        Node *node1 = extract_min(heap);
        Node *node2 = extract_min(heap);
        Node *parent = create_node('$', node1->freq + node2->freq);
        parent->left = node1;
        parent->right = node2;
        insert_heap(heap, parent);
    }
}

void free_tree(Node *root) {
    if (root != NULL) {
        free_tree(root->left);
        free_tree(root->right);
        free(root);
    }
}

Code *create_code(char symbol, int len) {
    Code *code = malloc(sizeof(Code));
    code->symbol = symbol;
    code->bits = malloc(len + 1);
    code->bits[len] = '\0';
    return code;
}

void traverse_tree(Node *node, char *prefix, int len, Code **codes, int *count) {
    if (node != NULL) {
        if (node->left == NULL && node->right == NULL) {
            codes[*count] = create_code(node->symbol, len);
            strcpy(codes[*count]->bits, prefix);
            (*count)++;
        } else {
            prefix[len] = '0';
            traverse_tree(node->left, prefix, len + 1, codes, count);
            prefix[len] = '1';
            traverse_tree(node->right, prefix, len + 1, codes, count);
        }
    }
}

void write_header(int fd, Node *root) {
    if (root != NULL) {
        if (root->left == NULL && root->right == NULL) {
            write(fd, "L", 1);
            write(fd, &root->symbol, 1);
        } else {
            write(fd, "N", 1);
            write_header(fd, root->left);
            write_header(fd, root->right);
        }
    }
}

Node *read_header(int fd) {
    char type;
    read(fd, &type, 1);
    if (type == 'L') {
        char symbol;
        read(fd, &symbol, 1);
        return create_node(symbol, 0);
    } else if (type == 'N') {
        Node *left = read_header(fd);
        Node *right = read_header(fd);
        Node *parent = create_node('$', 0);
        parent->left = left;
        parent->right = right;
        return parent;
    }
    return NULL;
}

void encode_file(int input_fd, int output_fd, Code **codes, int count) {
    char buffer;
    int bits_written = 0;
    char bits[9] = {'\0'};
    while (read(input_fd, &buffer, 1) > 0) {
        if (buffer >= 'A' && buffer <= 'Z') {
            int i;
            for (i = 0; i < count; i++) {
                if (codes[i]->symbol == buffer) {
                    int j;
                    for (j = 0; j < strlen(codes[i]->bits); j++) {
                        bits[bits_written++] = codes[i]->bits[j];
                        if (bits_written == 8) {
                            char encoded_byte = 0;
                            int k;
                            for (k = 0; k < 8; k++) {
                                encoded_byte <<= 1;
                                if (bits[k] == '1') {
                                    encoded_byte |= 1;
                                }
                            }
                            write(output_fd, &encoded_byte, 1);
                            bits_written = 0;
                            memset(bits, '\0', 9);
                        }
                    }
                    break;
                }
            }
        }
    }
    if (bits_written > 0) {
        char encoded_byte = 0;
        int k;
        for (k = 0; k < bits_written; k++) {
            encoded_byte <<= 1;
            if (bits[k] == '1') {
                encoded_byte |= 1;
            }
        }
        write(output_fd, &encoded_byte, 1);
    }
}

void decode_file(int input_fd, int output_fd, Node *root) {
    char buffer;
    Node *node = root;
    while (read(input_fd, &buffer, 1) > 0) {
        int i;
        for (i = 7; i >= 0; i--) {
            if ((buffer >> i) & 1) {
                node = node->right;
            } else {
                node = node->left;
            }
            if (node->left == NULL && node->right == NULL) {
                write(output_fd, &node->symbol, 1);
                node = root;
            }
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Usage: %s [c/d] [input_file] [output_file]\n", argv[0]);
        return 1;
    }

    char *mode = argv[1];
    char *input_file = argv[2];
    char *output_file = argv[3];

    if (strcmp(mode, "c") == 0) {
        int input_fd = open(input_file, O_RDONLY);
        if (input_fd == -1) {
            perror("Error opening input file");
            return 1;
        }

        int output_fd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        if (output_fd == -1) {
            perror("Error opening output file");
            return 1;
        }

        
        int freq[MAX_CHAR] = {0};
        char buffer;
        while (read(input_fd, &buffer, 1) > 0) {
            if (buffer >= 'A' && buffer <= 'Z') {
                freq[buffer]++;
            }
        }
        close(input_fd);

    
        Heap *heap = create_heap();
        int i;
        for (i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0) {
                Node *node = create_node(i, freq[i]);
                insert_heap(heap, node);
            }
        }
        build_tree(heap);
        Node *root = extract_min(heap);
        free(heap);

        Code *codes[MAX_CHAR] = {NULL};
        int count = 0;
        char prefix[MAX_CHAR] = {'\0'};
        traverse_tree(root, prefix, 0, codes, &count);

        
        int a;
        for (a = 0; a < count; a++) {
            printf("%c: %s\n", codes[a]->symbol, codes[a]->bits);
        }

        
        write_header(output_fd, root);

        
        input_fd = open(input_file, O_RDONLY);
        if (input_fd == -1) {
            perror("Error opening input file");
            return 1;
        }
        encode_file(input_fd, output_fd, codes, count);
        close(input_fd);
        close(output_fd);

        
        free_tree(root);
        for (i = 0; i < count; i++) {
            free(codes[i]->bits);
            free(codes[i]);
        }
    } else if (strcmp(mode, "d") == 0) {
        int input_fd = open(input_file, O_RDONLY);
        if (input_fd == -1) {
            perror("Error opening input file");
            return 1;
        }

        int output_fd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        if (output_fd == -1) {
            perror("Error opening output file");
            return 1;
        }

        
        Node *root = read_header(input_fd);

        
        decode_file(input_fd, output_fd, root);

        
        free_tree(root);

        close(input_fd);
        close(output_fd);
    } else {
        printf("Invalid mode: %s\n", mode);
        return 1;
    }

    return 0;
}
