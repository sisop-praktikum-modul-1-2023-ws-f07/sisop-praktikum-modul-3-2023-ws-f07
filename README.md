Kelompok F07 :
| Nama | NRP |
| ---------------------- | ---------- |
| Muhammad Arkan Karindra Darvesh | 5025211236 |
| Dany Dary | 5025211237 |
| Meyroja Jovancha Firoos | 5025211204 |


## Soal 1
### Deskripsi
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 

- Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
- Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
- Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
- Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
 * Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
Huruf A	: 01000001
Huruf a	: 01100001
Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
Agar lebih mudah, ubah semua huruf kecil ke huruf kapital.

### Code dan Penjelasan
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_CHAR 256

typedef struct node {
    char symbol;
    int freq;
    struct node *left;
    struct node *right;
} Node;

typedef struct heap {
    Node **arr;
    int size;
} Heap;

typedef struct code {
    char symbol;
    char *bits;
} Code;
``` 
1. Struct Node: Struktur data ini merepresentasikan sebuah node pada pohon Huffman. Setiap node memiliki simbol, frekuensi kemunculan simbol tersebut dalam data, serta dua pointer yang menunjuk ke anak kiri dan anak kanan dari node tersebut di dalam pohon.
2. Struct Heap: Struktur data ini merepresentasikan sebuah min heap yang digunakan untuk menyimpan node-node Huffman. Min heap merupakan struktur data berbasis array yang memastikan bahwa elemen terkecil selalu berada di posisi pertama dalam array.
3. Struct Code: Struktur data ini merepresentasikan kode biner untuk setiap simbol pada data yang akan dikompresi. Setiap kode biner disimpan dalam bentuk string karakter.

```c
void swap(Node **a, Node **b) {
    Node *tmp = *a;
    *a = *b;
    *b = tmp;
}
```
Fungsi swap di atas merupakan sebuah fungsi yang bertujuan untuk menukar dua buah pointer ke Node pada alamat memori yang berbeda. Fungsi swap menerima dua buah pointer ke pointer Node, yaitu a dan b.

```c
void heapify(Heap *heap, int i) {
    int smallest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < heap->size && heap->arr[left]->freq < heap->arr[smallest]->freq) {
        smallest = left;
    }

    if (right < heap->size && heap->arr[right]->freq < heap->arr[smallest]->freq) {
        smallest = right;
    }

    if (smallest != i) {
        swap(&heap->arr[i], &heap->arr[smallest]);
        heapify(heap, smallest);
    }
}
```
Code di atas adalah implementasi algoritma heapify dalam bahasa pemrograman C untuk mengurutkan elemen-elemen Heap pada suatu program yang menggunakan struktur data Heap.
Pertama-tama, sebuah Heap baru diinisialisasi dan diberikan parameter variabel i yang menunjukkan indeks dari elemen Heap yang akan di-heapify. Setelah itu, variabel smallest diinisialisasi dengan nilai i, dan variabel left dan right diinisialisasi dengan formula untuk menemukan indeks dari anak kiri dan anak kanan dari elemen dengan indeks i.
Kemudian dilakukan pengecekan untuk membandingkan nilai elemen pada indeks left dan right dengan elemen pada indeks smallest. Jika elemen pada indeks left atau right lebih kecil dari elemen pada indeks smallest, maka variabel smallest akan diupdate dengan nilai indeks yang lebih kecil tersebut.
Setelah itu, dilakukan pengecekan apakah nilai indeks smallest sama dengan nilai indeks i. Jika tidak sama, maka dilakukan pertukaran (swap) antara elemen pada indeks i dengan elemen pada indeks smallest dan kemudian fungsi heapify dijalankan lagi dengan parameter variabel smallest. Hal ini dilakukan untuk memastikan bahwa elemen yang baru ditukar masih memenuhi syarat dari struktur data Heap, yaitu elemen dengan nilai yang lebih kecil harus berada di atas elemen dengan nilai yang lebih besar.

```c 
Heap *create_heap() {
    Heap *heap = malloc(sizeof(Heap));
    heap->arr = malloc(MAX_CHAR * sizeof(Node *));
    heap->size = 0;
    return heap;
}

void insert_heap(Heap *heap, Node *node) {
    heap->arr[heap->size++] = node;

    int i = heap->size - 1;
    while (i > 0 && heap->arr[i]->freq < heap->arr[(i - 1) / 2]->freq) {
        swap(&heap->arr[i], &heap->arr[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}
```
Fungsi create_heap() dan insert_heap() di atas berfungsi untuk membuat dan memanipulasi struktur data Heap pada program Huffman coding.
Fungsi create_heap() digunakan untuk membuat Heap baru dan mengalokasikan memori untuk struktur tersebut. Fungsi ini mengembalikan pointer ke Heap yang telah dibuat. Pertama-tama, fungsi mengalokasikan memori untuk Heap menggunakan malloc(sizeof(Heap)). Selanjutnya, fungsi mengalokasikan memori untuk array of pointer Node yang akan digunakan sebagai wadah untuk menampung node-node pada Heap menggunakan malloc(MAX_CHAR * sizeof(Node *)). Nilai MAX_CHAR digunakan sebagai batas maksimum jumlah karakter yang mungkin terdapat pada data yang akan dikompresi. Terakhir, fungsi menginisialisasi nilai size pada Heap dengan nilai 0.
Fungsi insert_heap() digunakan untuk menambahkan sebuah node ke dalam Heap yang telah ada. Fungsi menerima dua parameter, yaitu pointer ke Heap dan pointer ke node yang akan dimasukkan. Pertama-tama, fungsi menambahkan node ke dalam Heap dengan menyalin alamat pointer node ke dalam array of pointer pada Heap. Selanjutnya, fungsi melakukan proses bubble up untuk menempatkan node pada posisi yang sesuai di dalam Heap. Proses bubble up dilakukan dengan membandingkan frekuensi node yang baru ditambahkan dengan frekuensi node di atasnya (parent) dalam Heap. Jika frekuensi node yang baru ditambahkan lebih kecil daripada frekuensi parent, maka kedua node tersebut ditukar posisinya dan proses ini diulang hingga node yang baru ditambahkan ditempatkan pada posisi yang tepat di dalam Heap.
Kedua fungsi tersebut digunakan untuk membantu dalam pembentukan pohon Huffman dan pengkodean karakter pada program Huffman
```c
Node *extract_min(Heap *heap) {
    Node *min = heap->arr[0];
    heap->arr[0] = heap->arr[--heap->size];
    heapify(heap, 0);
    return min;
}

Node *create_node(char symbol, int freq) {
    Node *node = malloc(sizeof(Node));
    node->symbol = symbol;
    node->freq = freq;
    node->left = NULL;
    node->right = NULL;
    return node;
}
```
Fungsi extract_min() dan create_node() di atas berfungsi untuk mengambil node dengan frekuensi kemunculan terkecil pada struktur data Heap dan membuat sebuah node baru pada pohon Huffman.
Fungsi extract_min() digunakan untuk mengambil node dengan frekuensi kemunculan terkecil pada Heap yang telah terurut. Fungsi mengembalikan pointer ke node yang telah diambil. Pertama-tama, fungsi menyimpan node pada posisi pertama pada Heap ke dalam variabel min. Kemudian, node pada posisi terakhir pada Heap dipindahkan ke posisi pertama dan ukuran Heap dikurangi 1. Selanjutnya, fungsi melakukan proses sink down (heapify) pada Heap dengan memanggil fungsi heapify(heap, 0) untuk memastikan bahwa elemen yang baru dipindahkan ke posisi pertama sudah berada pada posisi yang sesuai dalam Heap.
Fungsi create_node() digunakan untuk membuat sebuah node baru pada pohon Huffman. Fungsi menerima dua parameter, yaitu karakter symbol dan integer freq yang merepresentasikan frekuensi kemunculan karakter pada data yang akan dikompresi. Fungsi mengembalikan pointer ke Node yang baru dibuat. Pertama-tama, fungsi mengalokasikan memori untuk Node menggunakan malloc(sizeof(Node)). Selanjutnya, fungsi mengisi nilai-nilai pada node yang baru dibuat, yaitu karakter symbol, frekuensi freq, dan pointer left serta right yang awalnya diatur menjadi NULL.
Kedua fungsi tersebut digunakan pada proses pembentukan pohon Huffman dan pengkodean karakter pada program Huffman coding. Fungsi extract_min() digunakan pada proses penyusunan pohon Huffman dan fungsi create_node() digunakan pada proses pembentukan pohon Huffman dengan menggabungkan dua node yang frekuensi kemunculannya terkecil.
```c
void build_tree(Heap *heap) {
    while (heap->size > 1) {
        Node *node1 = extract_min(heap);
        Node *node2 = extract_min(heap);
        Node *parent = create_node('$', node1->freq + node2->freq);
        parent->left = node1;
        parent->right = node2;
        insert_heap(heap, parent);
    }
}

void free_tree(Node *root) {
    if (root != NULL) {
        free_tree(root->left);
        free_tree(root->right);
        free(root);
    }
}
```
Fungsi build_tree() dan free_tree() di atas berfungsi untuk membentuk pohon Huffman dari data yang akan dikompresi dan membebaskan memori yang telah dialokasikan untuk pohon Huffman setelah selesai digunakan.
Fungsi build_tree() digunakan untuk membangun pohon Huffman dari data yang akan dikompresi. Fungsi menerima parameter berupa pointer ke Heap yang berisi node-node yang merepresentasikan karakter pada data beserta frekuensi kemunculannya. Fungsi tidak mengembalikan nilai. Pertama-tama, fungsi melakukan loop while selama ukuran Heap lebih besar dari 1. Pada setiap iterasi, fungsi mengambil dua node dengan frekuensi kemunculan terkecil dari Heap menggunakan fungsi extract_min() dan menyatukannya dengan membuat sebuah node baru yang menjadi parent dari kedua node tersebut. Parent node yang baru dibuat memiliki karakter '$' dan frekuensi kemunculan yang merupakan jumlah frekuensi kemunculan dari kedua node yang disatukan. Selanjutnya, parent node yang baru dibuat dimasukkan kembali ke dalam Heap menggunakan fungsi insert_heap() sehingga Heap terus terurut berdasarkan frekuensi kemunculan node.
Fungsi free_tree() digunakan untuk membebaskan memori yang telah dialokasikan untuk pohon Huffman setelah selesai digunakan. Fungsi menerima parameter berupa pointer ke root node pada pohon Huffman. Fungsi tidak mengembalikan nilai. Pertama-tama, fungsi melakukan pemeriksaan apakah pointer root tidak bernilai NULL. Jika tidak, maka fungsi akan memanggil dirinya sendiri secara rekursif untuk menghapus node anak kiri dan anak kanan dari root. Setelah itu, fungsi akan membebaskan memori yang telah dialokasikan untuk root node itu sendiri menggunakan free(root).
```c
Code *create_code(char symbol, int len) {
    Code *code = malloc(sizeof(Code));
    code->symbol = symbol;
    code->bits = malloc(len + 1);
    code->bits[len] = '\0';
    return code;
}
```
Fungsi create_code() menerima dua parameter, yaitu karakter symbol dan integer len yang merepresentasikan panjang dari kode biner untuk karakter tersebut. Fungsi mengembalikan pointer ke struktur data Code yang baru dibuat. Pertama-tama, fungsi mengalokasikan memori untuk struktur data Code menggunakan malloc(sizeof(Code)). Selanjutnya, fungsi mengalokasikan memori untuk array of karakter yang merepresentasikan kode biner dengan ukuran len + 1 menggunakan malloc(len + 1). Penambahan 1 pada ukuran memori dialokasikan disebabkan untuk menampung karakter null terminator '\0' pada akhir string karakter kode biner. Terakhir, nilai symbol dan bits pada struktur data Code diisi dengan nilai-nilai yang sesuai, yaitu karakter symbol dan string karakter kode biner yang telah dialokasikan memori.
```c 
void traverse_tree(Node *node, char *prefix, int len, Code **codes, int *count) {
    if (node != NULL) {
        if (node->left == NULL && node->right == NULL) {
            codes[*count] = create_code(node->symbol, len);
            strcpy(codes[*count]->bits, prefix);
            (*count)++;
        } else {
            prefix[len] = '0';
            traverse_tree(node->left, prefix, len + 1, codes, count);
            prefix[len] = '1';
            traverse_tree(node->right, prefix, len + 1, codes, count);
        }
    }
}

void write_header(int fd, Node *root) {
    if (root != NULL) {
        if (root->left == NULL && root->right == NULL) {
            write(fd, "L", 1);
            write(fd, &root->symbol, 1);
        } else {
            write(fd, "N", 1);
            write_header(fd, root->left);
            write_header(fd, root->right);
        }
    }
}
```
Fungsi traverse_tree() digunakan untuk melakukan traverse pada pohon Huffman dari root ke setiap leaf node dan menghasilkan kode biner untuk setiap karakter pada data yang akan dikompresi. Fungsi menerima beberapa parameter, yaitu pointer ke node yang sedang ditinjau, string karakter prefix yang merepresentasikan kode biner untuk karakter tersebut, integer len yang merepresentasikan panjang dari string karakter prefix, array of pointer ke struktur data Code codes yang merepresentasikan kode biner untuk setiap karakter pada data, dan integer count yang merepresentasikan jumlah karakter yang telah diterjemahkan ke dalam kode biner. Fungsi tidak mengembalikan nilai. Pertama-tama, fungsi melakukan pemeriksaan apakah pointer node tidak bernilai NULL. Jika tidak, maka fungsi melakukan pemeriksaan apakah node merupakan leaf node. Jika iya, maka fungsi memanggil fungsi create_code() untuk membuat struktur data Code baru untuk karakter pada leaf node tersebut. Selanjutnya, string karakter kode biner pada struktur data Code yang baru dibuat diisi dengan string karakter prefix menggunakan fungsi strcpy(). Terakhir, jumlah karakter yang telah diterjemahkan ke dalam kode biner (*count) ditingkatkan sebanyak 1. Jika node bukan merupakan leaf node, maka fungsi memanggil dirinya sendiri secara rekursif untuk menghasilkan kode biner untuk setiap karakter pada node anak kiri dan node anak kanan. Pada saat melakukan traverse pada node anak kiri, string karakter prefix ditambahkan dengan karakter '0' dan len ditingkatkan sebanyak 1. Pada saat melakukan traverse pada node anak kanan, string karakter prefix ditambahkan dengan karakter '1' dan len ditingkatkan sebanyak 1.
Fungsi write_header() digunakan untuk menuliskan header dari file hasil kompresi pada file output. Fungsi menerima dua parameter, yaitu file descriptor fd untuk file output dan pointer ke root node pada pohon Huffman. Fungsi tidak mengembalikan nilai. Pertama-tama, fungsi melakukan pemeriksaan apakah pointer root tidak bernilai NULL. Jika iya, maka fungsi melakukan pemeriksaan apakah root merupakan leaf node. Jika iya, maka fungsi menuliskan karakter 'L' (leaf node) dan karakter symbol dari root node ke dalam file output menggunakan write(). Jika node bukan merupakan leaf node, maka fungsi menuliskan karakter 'N' (non-leaf node) ke dalam file output dan memanggil dirinya sendiri secara rekursif untuk menuliskan header dari node anak kiri dan node anak kanan.
```c
Node *read_header(int fd) {
    char type;
    read(fd, &type, 1);
    if (type == 'L') {
        char symbol;
        read(fd, &symbol, 1);
        return create_node(symbol, 0);
    } else if (type == 'N') {
        Node *left = read_header(fd);
        Node *right = read_header(fd);
        Node *parent = create_node('$', 0);
        parent->left = left;
        parent->right = right;
        return parent;
    }
    return NULL;
}
```
Fungsi read_header() menerima parameter berupa file descriptor fd untuk file hasil kompresi. Fungsi mengembalikan pointer ke root node pada pohon Huffman yang dibangun dari header. Pertama-tama, fungsi membaca karakter pertama dari file menggunakan read() dan menyimpannya ke dalam variabel type. Selanjutnya, fungsi melakukan pemeriksaan apakah karakter yang dibaca merupakan karakter 'L' (leaf node). Jika iya, maka fungsi membaca karakter symbol dari file menggunakan read() dan menyimpannya ke dalam variabel symbol. Fungsi selanjutnya memanggil fungsi create_node() untuk membuat node baru dengan karakter symbol dan frekuensi kemunculan 0. Fungsi mengembalikan pointer ke node yang baru dibuat. Jika karakter yang dibaca merupakan karakter 'N' (non-leaf node), maka fungsi memanggil dirinya sendiri secara rekursif untuk membaca header dari node anak kiri dan node anak kanan. Selanjutnya, fungsi memanggil fungsi create_node() untuk membuat node baru dengan karakter '$' dan frekuensi kemunculan 0 serta menyatukan node anak kiri dan node anak kanan dengan node baru tersebut. Fungsi mengembalikan pointer ke node yang baru dibuat. Jika karakter yang dibaca tidak sama dengan karakter 'L' atau 'N', maka fungsi mengembalikan NULL.
Fungsi read_header() digunakan pada proses dekompresi data pada program Huffman coding. Fungsi membantu dalam membaca header dari file hasil kompresi dan membangun pohon Huffman berdasarkan header yang dibaca sehingga data dapat direkonstruksi kembali menjadi data asli sebelum dikompresi.
```c
void encode_file(int input_fd, int output_fd, Code **codes, int count) {
    char buffer;
    int bits_written = 0;
    char bits[9] = {'\0'};
    while (read(input_fd, &buffer, 1) > 0) {
        if (buffer >= 'A' && buffer <= 'Z') {
            int i;
            for (i = 0; i < count; i++) {
                if (codes[i]->symbol == buffer) {
                    int j;
                    for (j = 0; j < strlen(codes[i]->bits); j++) {
                        bits[bits_written++] = codes[i]->bits[j];
                        if (bits_written == 8) {
                            char encoded_byte = 0;
                            int k;
                            for (k = 0; k < 8; k++) {
                                encoded_byte <<= 1;
                                if (bits[k] == '1') {
                                    encoded_byte |= 1;
                                }
                            }
                            write(output_fd, &encoded_byte, 1);
                            bits_written = 0;
                            memset(bits, '\0', 9);
                        }
                    }
                    break;
                }
            }
        }
    }
    if (bits_written > 0) {
        char encoded_byte = 0;
        int k;
        for (k = 0; k < bits_written; k++) {
            encoded_byte <<= 1;
            if (bits[k] == '1') {
                encoded_byte |= 1;
            }
        }
        write(output_fd, &encoded_byte, 1);
    }
}

void decode_file(int input_fd, int output_fd, Node *root) {
    char buffer;
    Node *node = root;
    while (read(input_fd, &buffer, 1) > 0) {
        int i;
        for (i = 7; i >= 0; i--) {
            if ((buffer >> i) & 1) {
                node = node->right;
            } else {
                node = node->left;
            }
            if (node->left == NULL && node->right == NULL) {
                write(output_fd, &node->symbol, 1);
                node = root;
            }
        }
    }
}
```
Fungsi encode_file() dan decode_file() di atas berfungsi untuk melakukan encoding dan decoding pada data yang akan dikompresi dan didekompresi menggunakan algoritma Huffman coding.
Fungsi encode_file() menerima tiga parameter, yaitu file descriptor input_fd untuk file input yang akan dikompresi, file descriptor output_fd untuk file output hasil kompresi, array of pointer ke struktur data Code codes yang merepresentasikan kode biner untuk setiap karakter pada data, dan integer count yang merepresentasikan jumlah karakter yang telah diterjemahkan ke dalam kode biner. Fungsi tidak mengembalikan nilai. Pertama-tama, fungsi membaca karakter pertama dari file input menggunakan read() dan menyimpannya ke dalam variabel buffer. Selanjutnya, fungsi melakukan pemeriksaan apakah karakter tersebut merupakan karakter huruf besar. Jika iya, maka fungsi melakukan iterasi pada array of pointer codes untuk mencari kode biner untuk karakter tersebut. Jika kode biner ditemukan, maka fungsi melakukan iterasi pada setiap karakter kode biner dan menambahkannya ke dalam array of karakter bits menggunakan variable bits_written sebagai penunjuk posisi saat ini dalam array. Jika jumlah karakter dalam array bits telah mencapai 8, maka fungsi mengkonversi 8 karakter terakhir dari array bits menjadi sebuah byte dan menuliskannya ke dalam file output menggunakan write(). Terakhir, variable bits_written diatur kembali menjadi 0 dan array bits dihapus isinya menggunakan fungsi memset(). Fungsi melanjutkan membaca karakter berikutnya dari file input dan melakukan operasi yang sama hingga mencapai akhir file. Jika jumlah karakter dalam array bits tidak mencapai 8 pada akhir file, maka fungsi mengkonversi karakter yang tersisa dari array bits menjadi sebuah byte dan menuliskannya ke dalam file output.
Fungsi decode_file() menerima tiga parameter, yaitu file descriptor input_fd untuk file input hasil kompresi, file descriptor output_fd untuk file output hasil dekompresi, dan pointer ke root node pada pohon Huffman yang telah dibangun dari header. Fungsi tidak mengembalikan nilai. Pertama-tama, fungsi membaca byte pertama dari file input menggunakan read() dan menyimpannya ke dalam variabel buffer. Selanjutnya, fungsi melakukan iterasi pada setiap bit pada byte buffer secara berurutan dari bit yang paling signifikan (leftmost) hingga bit yang paling tidak signifikan (rightmost). Jika bit tersebut sama dengan 1, maka fungsi menavigasi ke node anak kanan pada pohon Huffman. Jika bit tersebut sama dengan 0, maka fungsi menavigasi ke node anak kiri pada pohon Huffman. Fungsi kemudian memeriksa apakah node yang ditemukan merupakan leaf node pada pohon Huffman. Jika iya, maka fungsi menuliskan karakter symbol dari node tersebut ke dalam file output menggunakan write() dan menavigasi kembali ke root node pada pohon Huffman. Fungsi melanjutkan membaca byte berikutnya dari file input dan melakukan operasi yang sama hingga mencapai akhir file.
Fungsi encode_file() dan decode_file() digunakan pada proses kompresi dan dekompresi.
```c
int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Usage: %s [c/d] [input_file] [output_file]\n", argv[0]);
        return 1;
    }

    char *mode = argv[1];
    char *input_file = argv[2];
    char *output_file = argv[3];

    if (strcmp(mode, "c") == 0) {
        int input_fd = open(input_file, O_RDONLY);
        if (input_fd == -1) {
            perror("Error opening input file");
            return 1;
        }

        int output_fd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        if (output_fd == -1) {
            perror("Error opening output file");
            return 1;
        }

        
        int freq[MAX_CHAR] = {0};
        char buffer;
        while (read(input_fd, &buffer, 1) > 0) {
            if (buffer >= 'A' && buffer <= 'Z') {
                freq[buffer]++;
            }
        }
        close(input_fd);

    
        Heap *heap = create_heap();
        int i;
        for (i = 0; i < MAX_CHAR; i++) {
            if (freq[i] > 0) {
                Node *node = create_node(i, freq[i]);
                insert_heap(heap, node);
            }
        }
        build_tree(heap);
        Node *root = extract_min(heap);
        free(heap);

        Code *codes[MAX_CHAR] = {NULL};
        int count = 0;
        char prefix[MAX_CHAR] = {'\0'};
        traverse_tree(root, prefix, 0, codes, &count);

        
        int a;
        for (a = 0; a < count; a++) {
            printf("%c: %s\n", codes[a]->symbol, codes[a]->bits);
        }

        
        write_header(output_fd, root);

        
        input_fd = open(input_file, O_RDONLY);
        if (input_fd == -1) {
            perror("Error opening input file");
            return 1;
        }
        encode_file(input_fd, output_fd, codes, count);
        close(input_fd);
        close(output_fd);

        
        free_tree(root);
        for (i = 0; i < count; i++) {
            free(codes[i]->bits);
            free(codes[i]);
        }
    } else if (strcmp(mode, "d") == 0) {
        int input_fd = open(input_file, O_RDONLY);
        if (input_fd == -1) {
            perror("Error opening input file");
            return 1;
        }

        int output_fd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        if (output_fd == -1) {
            perror("Error opening output file");
            return 1;
        }

        
        Node *root = read_header(input_fd);

        
        decode_file(input_fd, output_fd, root);

        
        free_tree(root);

        close(input_fd);
        close(output_fd);
    } else {
        printf("Invalid mode: %s\n", mode);
        return 1;
    }

    return 0;
}
```
Code tersebut adalah implementasi dari algoritma Huffman encoding dan decoding yang digunakan untuk kompresi dan dekompresi teks. Program ini menerima tiga argumen pada command line: mode (c/d) untuk kompresi atau dekompresi, nama file input, dan nama file output. Jika mode c, maka program akan membaca file input, menghitung frekuensi karakter, membangun pohon Huffman, dan menuliskan header dan file output yang telah dikompressi. Jika mode d, maka program akan membaca file input, membaca header untuk membangun pohon Huffman, dan menuliskan file output yang telah didekompresi.


## Soal 2

### Deskripsi

Soal ini meminta kamu untuk membuat tiga program C yang berbeda dengan spesifikasi berikut:

- Program pertama bernama kalian.c, yang berfungsi untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks adalah angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Program harus menampilkan matriks hasil perkalian tadi ke layar.

- Program kedua bernama cinta.c, yang berfungsi untuk mengambil variabel hasil perkalian matriks dari program kalian.c dan menampilkannya ke layar. Setelah ditampilkan, program harus menghitung faktorial untuk setiap angka dari matriks tersebut dan menampilkan hasilnya ke layar dengan format seperti matriks.

- Program ketiga bernama sisop.c, yang memiliki spesifikasi yang sama dengan program cinta.c, namun tidak menggunakan thread dan multithreading. Tujuan program ini adalah untuk membandingkan performa antara program dengan multithread dan program tanpa multithread.

Dalam menyelesaikan tugas ini, kamu harus menerapkan konsep shared memory untuk mengirimkan variabel hasil perkalian matriks dari program kalian.c ke program cinta.c. Selain itu, kamu juga harus menerapkan konsep thread dan multithreading dalam program cinta.c untuk menghitung faktorial.

### Code dan Penjelasan

#### kalian.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int main() {
    int mat1[ROW1][COL1], mat2[ROW2][COL2], product[ROW1][COL2];
    int i, j, k;

    // Inisialisasi matriks dengan angka random
    srand(time(NULL));
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            mat1[i][j] = rand() % 5 + 1;
        }
    }
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            mat2[i][j] = rand() % 4 + 1;
        }
    }

    // Hitung perkalian matriks
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            product[i][j] = 0;
            for (k = 0; k < ROW2; k++) {
                product[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    // Tampilkan matriks hasil perkalian
    printf("Hasil perkalian matriks:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d ", product[i][j]);
        }
        printf("\n");
    }

    // Buat shared memory untuk hasil perkalian matriks
    const char *shared_mem_name = "MySharedMemory1";
    int shm_fd = shm_open(shared_mem_name, O_CREAT | O_RDWR, 0666);
    if (shm_fd == -1) {
        printf("Error creating shared memory.\n");
        return 1;
    }
    ftruncate(shm_fd, sizeof(product));
    int *shared_mem = mmap(0, sizeof(product), PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shared_mem == MAP_FAILED) {
        printf("Error mapping shared memory.\n");
        shm_unlink(shared_mem_name);
        return 1;
    }

    // Salin hasil perkalian matriks ke shared memory
    memcpy(shared_mem, product, sizeof(product));

    // Tutup handle dan unmap shared memory
    munmap(shared_mem, sizeof(product));
    close(shm_fd);

    return 0;
}
```
Program di atas merupakan program untuk melakukan perkalian matriks, menampilkan hasil perkalian matriks, dan menyimpan hasil perkalian matriks ke dalam shared memory.

Pertama-tama, program melakukan inisialisasi matriks `mat1` dan `mat2` dengan angka random menggunakan fungsi `rand()`. Angka yang dihasilkan dibatasi antara 1 hingga 5 untuk `mat1` dan antara 1 hingga 4 untuk `mat2`.

Selanjutnya, program menghitung hasil perkalian matriks `mat1` dan `mat2` menggunakan nested loop dengan variabel i, j, dan k. Hasil perkalian disimpan ke dalam matriks `product`.

Setelah itu, program menampilkan matriks hasil perkalian ke layar menggunakan nested loop dengan variabel i dan j.

Selanjutnya, program membuat shared memory dengan nama `MySharedMemory1` menggunakan fungsi `shm_open()`. Jika berhasil, program melakukan truncation pada shared memory sebesar ukuran product. Kemudian, program memetakan shared memory ke dalam memori menggunakan fungsi `mmap()` dengan proteksi write dan flag `MAP_SHARED`. Jika berhasil, program menyalin isi `product` ke shared memory menggunakan fungsi `memcpy()`.

Terakhir, program menutup handle shared memory dan unmap shared memory menggunakan fungsi `munmap()`, serta menutup file descriptor shared memory menggunakan fungsi `close()`.

Program kemudian mengembalikan nilai 0 sebagai tanda program berhasil dijalankan.

#### cinta.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h> 
#include <unistd.h>
#include <string.h>
#include <time.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5
#define MAX_THREADS ROW1*COL2

int product[ROW1][COL2];
int fact[ROW1][COL2];

void *factorial(void *arg) {
    int *num = (int *)arg;
    int f = 1;
    int i;
    for (i = 2; i <= *num; i++) {
        f *= i;
    }
    *num = f;
    return NULL;
}

int main() {
    clock_t start_time, end_time;
    start_time = clock();
    int i, j;
    int shm_fd1, shm_fd2;
    int *shared_mem1, *shared_mem2;
    const char *shared_mem_name1 = "MySharedMemory1";
    const char *shared_mem_name2 = "MySharedMemory2";

    srand(time(NULL));
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            product[i][j] = rand() % 5 + 1;
        }
    }
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            product[i][j] = rand() % 4 + 1;
        }
    }

    shm_fd1 = shm_open(shared_mem_name1, O_CREAT | O_RDWR, 0666);
    if (shm_fd1 == -1) {
        printf("Error creating shared memory.\n");
        return 1;
    }
    ftruncate(shm_fd1, sizeof(product));
    shared_mem1 = mmap(0, sizeof(product), PROT_WRITE, MAP_SHARED, shm_fd1, 0);
    memcpy(shared_mem1, product, sizeof(product));

    shm_fd2 = shm_open(shared_mem_name2, O_CREAT | O_RDWR, 0666);
    if (shm_fd2 == -1) {
        printf("Error creating shared memory.\n");
        shm_unlink(shared_mem_name1);
        return 1;
    }
    ftruncate(shm_fd2, sizeof(fact));
    shared_mem2 = mmap(0, sizeof(fact), PROT_WRITE, MAP_SHARED, shm_fd2, 0);

    pthread_t threads[MAX_THREADS];
    int thread_args[MAX_THREADS];
    int thread_count = 0;
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            thread_args[thread_count] = product[i][j];
            pthread_create(&threads[thread_count], NULL, factorial, &thread_args[thread_count]);
            thread_count++;
        }
    }

    for (i = 0; i < MAX_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

     for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            fact[i][j] = thread_args[i * COL2 + j];
        }
    }

    memcpy(shared_mem2, fact, sizeof(fact));

    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d\t", shared_mem1[i * COL2 + j]);
        }
        printf("\n");
    }

    printf("\nMatriks hasil faktorial:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d\t", fact[i][j]);
        }
        printf("\n");
    }

    munmap(shared_mem1, sizeof(product));
    munmap(shared_mem2, sizeof(fact));
    shm_unlink(shared_mem_name1);
    shm_unlink(shared_mem_name2);

    end_time = clock();  
    double elapsed_time = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang diperlukan: %.3f s\n", elapsed_time);
    
    return 0;
}
```
Kode di atas adalah program C yang menghitung hasil perkalian matriks secara parallel dengan menggunakan shared memory dan juga menghitung faktorial dari setiap elemen matriks hasil perkalian.

Pada awal program, beberapa header file di-include, yaitu stdio.h, stdlib.h, pthread.h, sys/mman.h, sys/stat.h, fcntl.h, unistd.h, dan string.h. Selanjutnya, beberapa konstanta didefinisikan, yaitu ROW1, COL1, ROW2, COL2, dan MAX_THREADS. ROW1 dan COL1 merepresentasikan ukuran matriks pertama (4 baris dan 2 kolom), sedangkan ROW2 dan COL2 merepresentasikan ukuran matriks kedua (2 baris dan 5 kolom). MAX_THREADS merepresentasikan jumlah thread yang akan dibuat, yaitu sama dengan jumlah elemen pada matriks hasil perkalian.

Selanjutnya, ada dua array dua dimensi, yaitu product dan fact, yang akan digunakan untuk menyimpan matriks hasil perkalian dan matriks hasil faktorial, masing-masing. Kemudian, ada fungsi factorial yang akan digunakan sebagai fungsi yang akan dijalankan oleh thread untuk menghitung faktorial dari suatu bilangan.

Pada main function, pertama-tama beberapa variabel dideklarasikan, yaitu i, j, shm_fd1, shm_fd2, shared_mem1, shared_mem2, shared_mem_name1, dan shared_mem_name2. shared_mem_name1 dan shared_mem_name2 adalah nama shared memory yang akan digunakan untuk menyimpan matriks hasil perkalian dan matriks hasil faktorial, masing-masing.

Kemudian, pada bagian berikutnya, matriks product diisi dengan angka-angka random antara 1 hingga 5, sedangkan matriks fact belum diisi dengan apa-apa. Selanjutnya, shared memory untuk product dan fact dibuat dengan menggunakan shm_open, kemudian di-mmap agar bisa diakses oleh thread. Setelah itu, array thread_args dan threads dideklarasikan, kemudian variabel thread_count diinisialisasi dengan 0. Array thread_args akan digunakan untuk menyimpan bilangan yang akan dihitung faktorialnya, sedangkan array threads akan digunakan untuk menyimpan ID thread.

Setelah itu, dilakukan nested loop untuk mengisi array thread_args dan juga untuk membuat thread sebanyak MAX_THREADS. Pada setiap iterasi, nilai dari product[i][j] akan disimpan pada thread_args[thread_count], kemudian thread baru dibuat dengan menggunakan pthread_create. Variabel thread_count di-increment pada setiap iterasi.

Setelah seluruh thread selesai dijalankan dengan menggunakan pthread_join, nilai-nilai faktorial yang sudah dihitung oleh masing-masing thread akan disimpan kembali pada array fact. Nilai-nilai faktorial tersebut sebelumnya disimpan pada array thread_args, yang disimpan sebagai argumen thread. Pada bagian ini, nilai faktorial yang sudah dihitung akan disimpan pada posisi yang sama pada array fact.

Selanjutnya dilakukan inisialisasi shared memory dengan menggunakan fungsi shm_open() dan memberikan nama shared memory pada shared_mem_name1 dan shared_mem_name2. Kemudian, dilakukan pemanggilan ftruncate() untuk menentukan ukuran dari shared memory dan membuat shared memory siap digunakan.

Selanjutnya, dilakukan pemetaan ke alamat virtual menggunakan fungsi mmap() dengan parameter MAP_SHARED, sehingga kedua proses bisa mengakses shared memory tersebut. Pada contoh kode ini, pertama-tama data matriks product dicopy ke dalam shared memory shared_mem1. Kemudian, matriks tersebut akan dikalikan dengan faktorial masing-masing elemennya.

Setelah data faktorial didapatkan, hasilnya akan disimpan ke dalam array fact. Kemudian, array fact tersebut juga disalin ke dalam shared memory shared_mem2. Terakhir, kedua shared memory tersebut di-unmap menggunakan munmap() dan di-unlink menggunakan shm_unlink().

Pada akhir program, dihitung waktu eksekusi dengan menghitung perbedaan antara start_time dan end_time. Kemudian, hasilnya akan ditampilkan pada layar bersama dengan matriks hasil perkalian dan matriks hasil faktorial.

#### sisop.c
Kode ini menggunakan shared memory untuk menghitung hasil perkalian matriks dan faktorial dari setiap elemen hasil perkalian tersebut. 
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5

int product[ROW1][COL2]; // shared memory untuk hasil perkalian matriks
int fact[ROW1][COL2];    // shared memory untuk hasil faktorial

// Fungsi untuk menghitung faktorial
int factorial(int num)
{
    int f = 1;
    int i;
    for (i = 2; i <= num; i++)
    {
        f *= i;
    }
    return f;
}

int main()
{
    clock_t start_time, end_time;
    start_time = clock();

    int i, j;
    int shm_fd1, shm_fd2;
    int *shared_mem1, *shared_mem2;
    const char *shared_mem_name1 = "MySharedMemory1";
    const char *shared_mem_name2 = "MySharedMemory2";

    // Inisialisasi matriks dengan angka random
    srand(time(NULL));
    for (i = 0; i < ROW1; i++)
    {
        for (j = 0; j < COL1; j++)
        {
            product[i][j] = rand() % 5 + 1;
        }
    }
    for (i = 0; i < ROW2; i++)
    {
        for (j = 0; j < COL2; j++)
        {
            product[i][j] = rand() % 4 + 1;
        }
    }

    // Buka shared memory untuk hasil perkalian matriks
    shm_fd1 = shm_open(shared_mem_name1, O_CREAT | O_RDWR, 0666);
    if (shm_fd1 == -1)
    {
        printf("Error creating shared memory.\n");
        return 1;
    }
    ftruncate(shm_fd1, sizeof(product));
    shared_mem1 = mmap(0, sizeof(product), PROT_WRITE, MAP_SHARED, shm_fd1, 0);
    if (shared_mem1 == MAP_FAILED)
    {
        printf("Error mapping shared memory.\n");
        close(shm_fd1);
        return 1;
    }
    // Salin hasil perkalian matriks ke shared memory
    memcpy(shared_mem1, product, sizeof(product));

    // Buka shared memory untuk hasil faktorial
    shm_fd2 = shm_open(shared_mem_name2, O_CREAT | O_RDWR, 0666);
    if (shm_fd2 == -1)
    {
        printf("Error creating shared memory.\n");
        close(shm_fd1);
        return 1;
    }
    ftruncate(shm_fd2, sizeof(fact));
    shared_mem2 = mmap(0, sizeof(fact), PROT_WRITE, MAP_SHARED, shm_fd2, 0);
    if (shared_mem2 == MAP_FAILED)
    {
        printf("Error mapping shared memory.\n");
        close(shm_fd1);
        close(shm_fd2);
        return 1;
    }
    for (i = 0; i < ROW1; i++)
    {
        for (j = 0; j < COL2; j++)
        {
            int num = product[i][j];
            int f = factorial(num);
            fact[i][j] = f;
        }
    }
    // Cetak matriks hasil perkalian dan faktorial
    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < ROW1; i++)
    {
        for (j = 0; j < COL2; j++)
        {
            printf("%d ", shared_mem1[i * COL2 + j]);
        }
        printf("\n");
    }
    printf("\nMatriks hasil faktorial:\n");
    for (i = 0; i < ROW1; i++)
    {
        for (j = 0; j < COL2; j++)
        {
            printf("%d ", fact[i][j]);
        }
        printf("\n");
    }

    // Tutup shared memory
    munmap(shared_mem1, sizeof(product));
    munmap(shared_mem2, sizeof(fact));
    close(shm_fd1);
    close(shm_fd2);
    shm_unlink(shared_mem_name1);
    shm_unlink(shared_mem_name2);

    end_time = clock();
    double elapsed_time = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    printf("Waktu yang diperlukan: %.3f s\n", elapsed_time);

    return 0;
}
```
Berikut adalah penjelasan singkat dari setiap bagian program:

`#include` digunakan untuk memasukkan library-library yang akan digunakan dalam program.
`#define` digunakan untuk mendefinisikan konstanta ROW1, COL1, ROW2, dan COL2.
`product` dan `fact` adalah array 2D yang digunakan untuk menyimpan hasil perkalian matriks dan hasil faktorial.
factorial adalah fungsi untuk menghitung faktorial dari suatu angka.
`main` adalah fungsi utama dalam program.
`start_time` dan `end_time` adalah variabel yang digunakan untuk menghitung waktu yang diperlukan oleh program untuk menyelesaikan tugasnya.
`shm_fd1`, `shm_fd2`, `shared_mem1`, dan `shared_mem2` adalah variabel yang digunakan untuk mengakses shared memory.
`shared_mem_name1` dan `shared_mem_name2` adalah nama shared memory yang digunakan dalam program.
`srand` digunakan untuk menginisialisasi nilai random untuk elemen matriks.
`shm_open` digunakan untuk membuka shared memory yang akan digunakan dalam program.
`ftruncate` digunakan untuk menentukan ukuran shared memory.
`mmap` digunakan untuk memetakan shared memory ke dalam memori program.
`memcpy` digunakan untuk menyalin hasil perkalian matriks ke shared memory.
`for` digunakan untuk menghitung hasil faktorial dari setiap elemen hasil perkalian matriks dan menyimpannya ke dalam array fact.
`printf` digunakan untuk mencetak hasil perkalian matriks dan hasil faktorial ke layar.
`munmap`, close, dan shm_unlink digunakan untuk menutup shared memory setelah digunakan.
`end_time` - `start_time` digunakan untuk menghitung waktu yang diperlukan oleh program untuk menyelesaikan tugasnya.

Setelah membuat shared memory, program kemudian menginisialisasi matriks `product` dan `fact`. Matriks `product` diisi dengan angka acak dengan menggunakan fungsi `rand()`, sedangkan matriks `fact` belum diisi dan akan dihitung kemudian.

Kemudian program menyalin isi matriks product ke dalam shared memory yang telah dibuka sebelumnya dengan menggunakan fungsi `memcpy()`. Setelah itu, program melakukan perhitungan faktorial untuk setiap elemen di matriks product dan menyimpan hasilnya di matriks `fact`.

Setelah itu, program mencetak matriks hasil perkalian dan matriks hasil faktorial dengan menggunakan data yang ada di shared memory dan matriks `fact`. Setelah selesai, program menutup shared memory dan melakukan `shm_unlink()` untuk menghapus shared memory.

Terakhir, program mencatat waktu yang diperlukan untuk menjalankan program dengan menggunakan fungsi `clock()`, menghitung selisih waktu sejak awal program dan waktu selesai, dan mencetaknya sebagai output.
